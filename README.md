UX/UI design for a pricing page for the [Globuzzer's website](https://globuzzer.com/). Technical exercise proposed by Globuzzer as part of a recruitment process.

All the icons have been taken from [iconmonstr](https://iconmonstr.com/) and are used in this project for non profit purposes.


*  Link to Figma project: https://www.figma.com/file/cZBB15zNIevbtYI8BkNqMN/Globuzzer-pricing-page


*  [PNG Screenshot of the project](globuzzer_price_page_design.png)